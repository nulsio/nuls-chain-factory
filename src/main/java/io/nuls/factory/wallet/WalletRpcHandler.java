package io.nuls.factory.wallet;

import io.nuls.factory.ApiContext;
import io.nuls.factory.rpc.result.JsonRpcRequest;
import io.nuls.factory.rpc.result.RpcResult;
import io.nuls.factory.rpc.result.RpcResultError;
import io.nuls.factory.util.HttpClientUtils;
import io.nuls.factory.util.JsonUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WalletRpcHandler {

    public static RpcResult validateTx(String url, String txHex) throws Exception {
        JsonRpcRequest rpcRequest = new JsonRpcRequest();
        rpcRequest.setMethod("validateTx");
        List<Object> params = new ArrayList<>();
        params.add(ApiContext.wallet_chain_id);
        params.add(txHex);
        rpcRequest.setParams(params);
        String response = HttpClientUtils.postJson(url, rpcRequest);
        return toRpcResult(response);
    }

    public static RpcResult getTx(String url, String txHash) throws Exception {
        JsonRpcRequest rpcRequest = new JsonRpcRequest();
        rpcRequest.setMethod("getTx");
        List<Object> params = new ArrayList<>();
        params.add(ApiContext.wallet_chain_id);
        params.add(txHash);
        rpcRequest.setParams(params);
        String response = HttpClientUtils.postJson(url, rpcRequest);
        return toRpcResult(response);
    }


    private static RpcResult toRpcResult(String response) throws Exception {
        RpcResult rpcResult = new RpcResult();
        Map<String, Object> map = JsonUtils.json2map(response);
        if (map.get("result") != null) {
            rpcResult.setSuccess(true);
            rpcResult.setResult(map.get("result"));
        } else {
            rpcResult.setSuccess(false);
            Map<String, Object> errorMap = (Map<String, Object>) map.get("error");
            RpcResultError error = new RpcResultError();
            error.setCode((String) errorMap.get("code"));
            error.setMessage((String) errorMap.get("message"));
            error.setData(errorMap.get("data"));
        }
        return rpcResult;
    }

    public static void main(String[] args) {
        try {
            ApiContext.wallet_chain_id = 100;
            RpcResult rpcResult = getTx("http://192.168.1.192:18003/", "60efbed577ce0360186c9abc2f63b0f20dfaf484be496ce27e63ca3069dfd618");
            System.out.println(rpcResult);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
