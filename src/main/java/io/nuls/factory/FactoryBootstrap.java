package io.nuls.factory;

import io.nuls.factory.constants.Constant;
import io.nuls.factory.util.Log;
import io.nuls.factory.util.PropertiesUtils;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.ServerConfiguration;
import org.glassfish.grizzly.nio.transport.TCPNIOTransport;
import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;
import org.glassfish.grizzly.strategies.WorkerThreadIOStrategy;
import org.glassfish.grizzly.threadpool.ThreadPoolConfig;
import org.glassfish.grizzly.utils.Charsets;
import org.glassfish.jersey.internal.guava.ThreadFactoryBuilder;
import org.glassfish.jersey.servlet.ServletContainer;


public class FactoryBootstrap {

    public static void main(String[] args) {
        loadConfig();
        start();
    }

    private static void loadConfig() {
        PropertiesUtils.init("cfg.properties");
        ApiContext.ip = PropertiesUtils.readString("ip", "0.0.0.0");
        ApiContext.port = PropertiesUtils.readInt("port", 8080);
        ApiContext.wallet_ip = PropertiesUtils.readString("wallet_ip", "127.0.0.1");
        ApiContext.wallet_port = PropertiesUtils.readInt("wallet_port", 18003);
        ApiContext.wallet_chain_id = PropertiesUtils.readInt("wallet_chain_id", 1);
    }

//    private static void start() {
//        try {
//            SpringLiteContext.init("io.nuls.factory");
//
//            JsonRpcServer server = new JsonRpcServer();
//            server.startServer(ApiContext.ip, ApiContext.port);
//
//            while (true) {
//                try {
//                    Thread.sleep(60000L);
//                } catch (InterruptedException e) {
//                    Log.error(e);
//                }
//            }
//        } catch (Exception e) {
//            Log.error(e);
//        }
//    }

    private static void start() {
        // Create web application context.
        WebappContext webappContext = new WebappContext("NULS-CHAIN-FACTORY", "/nuls");
        webappContext.addContextInitParameter("contextClass", "org.springframework.web.context.support.XmlWebApplicationContext");
        webappContext.addContextInitParameter("contextConfigLocation", "classpath:spring-config.xml");
        webappContext.addListener("org.springframework.web.context.ContextLoaderListener");

        // Create a servlet registration for the web application in order to wire up Spring managed collaborators to Jersey resources.
        ServletRegistration servletRegistration = webappContext.addServlet("jersey-servlet", ServletContainer.class);
        servletRegistration.setInitParameter("javax.ws.rs.Application", "io.nuls.factory.filter.NulsResourceConfig");
        servletRegistration.setInitParameter("jersey.config.server.provider.packages", Constant.RPC_PACKAGES);
        servletRegistration.addMapping("/*");

        HttpServer httpServer = new HttpServer();
        NetworkListener listener = new NetworkListener("NULS-FACTORY-RPC", ApiContext.ip, ApiContext.port);
        TCPNIOTransport transport = listener.getTransport();
        ThreadPoolConfig workerPool = ThreadPoolConfig.defaultConfig()
                .setCorePoolSize(8)
                .setMaxPoolSize(8)
                .setQueueLimit(2000)
                .setThreadFactory((new ThreadFactoryBuilder()).setNameFormat("grizzly-http-server-%d").build());
        transport.configureBlocking(false);
        transport.setSelectorRunnersCount(2);
        transport.setWorkerThreadPoolConfig(workerPool);
        transport.setIOStrategy(WorkerThreadIOStrategy.getInstance());
        transport.setTcpNoDelay(true);
        listener.setSecure(false);
        httpServer.addListener(listener);

        ServerConfiguration config = httpServer.getServerConfiguration();
        config.setDefaultQueryEncoding(Charsets.UTF8_CHARSET);

        webappContext.deploy(httpServer);

        try {
            httpServer.start();
        } catch (Exception e) {
            Log.error(e);
            httpServer.shutdownNow();
        }
    }

}
