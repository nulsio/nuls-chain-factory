package io.nuls.factory.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtils {
    private static Properties properties = new Properties();

    public static boolean init(String fileName) {
        InputStream in = null;
        try {
            in = PropertiesUtils.class.getClassLoader().getResourceAsStream(fileName);
            properties.load(in);
            return true;
        } catch (Exception e) {
            Log.error(e);
            return false;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public static String readString(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    public static String readString(String property) {
        return properties.getProperty(property);
    }

    public static int readInt(String key, int defaultValue) {
        String value = properties.getProperty(key);
        if (value == null) {
            return defaultValue;
        }
        return Integer.parseInt(value);
    }
}
