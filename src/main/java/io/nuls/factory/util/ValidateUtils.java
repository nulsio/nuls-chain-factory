package io.nuls.factory.util;

import io.nuls.factory.constants.Constant;

public class ValidateUtils {

    public static final String MAINNET_PREFIX = Constant.MAINNET_DEFAULT_ADDRESS_PREFIX;

    public static boolean validateAddress(String address) {
        if (StringUtils.isBlank(address)) {
            return false;
        }
        if (!address.startsWith(MAINNET_PREFIX)) {
            return false;
        }
        return true;
    }

    public static boolean validateHash(String hash) {
        if (StringUtils.isBlank(hash)) {
            return false;
        }
        if (hash.length() < 20 || hash.length() > 40) {
            return false;
        }
        return true;
    }
}
