package io.nuls.factory.constants;

public interface Constant {

    String RPC_PACKAGES = "io.nuls.factory.controller";
    String MAINNET_DEFAULT_ADDRESS_PREFIX = "NULS";
}
