package io.nuls.factory.model;

import java.util.Date;

public class ChainInfo {

    private Integer chainId;

    private String chainName;

    private String address;

    private Integer type;

    private String logo;

    private String seeds;

    private Boolean isCross;

    private Long payTimes;

    private Integer nodeCount;

    private Long payAmount;

    private Date createTime;

    private Integer status;

    private String genesisInfo;

    public Integer getChainId() {
        return chainId;
    }

    public void setChainId(Integer chainId) {
        this.chainId = chainId;
    }

    public String getChainName() {
        return chainName;
    }

    public void setChainName(String chainName) {
        this.chainName = chainName == null ? null : chainName.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo == null ? null : logo.trim();
    }

    public String getSeeds() {
        return seeds;
    }

    public void setSeeds(String seeds) {
        this.seeds = seeds == null ? null : seeds.trim();
    }

    public Boolean getIsCross() {
        return isCross;
    }

    public void setIsCross(Boolean isCross) {
        this.isCross = isCross;
    }

    public Long getPayTimes() {
        return payTimes;
    }

    public void setPayTimes(Long payTimes) {
        this.payTimes = payTimes;
    }

    public Integer getNodeCount() {
        return nodeCount;
    }

    public void setNodeCount(Integer nodeCount) {
        this.nodeCount = nodeCount;
    }

    public Long getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Long payAmount) {
        this.payAmount = payAmount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGenesisInfo() {
        return genesisInfo;
    }

    public void setGenesisInfo(String genesisInfo) {
        this.genesisInfo = genesisInfo == null ? null : genesisInfo.trim();
    }
}