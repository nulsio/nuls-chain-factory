package io.nuls.factory.model;

import java.util.Date;

public class AccountInfo {

    private String address;

    private Date createTime;

    private Long balance;

    private Boolean isRegisterChain;

    private Boolean isUploadModule;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Boolean getIsRegisterChain() {
        return isRegisterChain;
    }

    public void setIsRegisterChain(Boolean isRegisterChain) {
        this.isRegisterChain = isRegisterChain;
    }

    public Boolean getIsUploadModule() {
        return isUploadModule;
    }

    public void setIsUploadModule(Boolean isUploadModule) {
        this.isUploadModule = isUploadModule;
    }
}