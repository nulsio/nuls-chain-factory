package io.nuls.factory.service;


import io.nuls.factory.dao.mapper.AccountInfoMapper;
import io.nuls.factory.model.AccountInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AccountService {

    @Autowired
    private AccountInfoMapper accountInfoMapper;

    public AccountInfo save(AccountInfo accountInfo) {
        accountInfo.setCreateTime(new Date());
        accountInfo.setBalance(0L);
        accountInfo.setIsRegisterChain(false);
        accountInfo.setIsUploadModule(false);
        accountInfoMapper.insert(accountInfo);
        return accountInfo;
    }

    public AccountInfo getAccount(String address) {
        return accountInfoMapper.selectByPrimaryKey(address);
    }
}
