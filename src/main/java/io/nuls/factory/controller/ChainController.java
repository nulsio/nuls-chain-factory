package io.nuls.factory.controller;

import io.nuls.factory.rpc.form.ChainForm;
import io.nuls.factory.rpc.result.RpcResult;
import io.nuls.factory.util.StringUtils;
import io.nuls.factory.util.ValidateUtils;
import org.springframework.stereotype.Component;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/chain")
@Component
public class ChainController {

    @POST
    @Path("/selectByAccount")
    @Produces(MediaType.APPLICATION_JSON)
    public RpcResult selectAccountChains() {
        return null;
    }

    @POST
    @Path("/register")
    @Produces(MediaType.APPLICATION_JSON)
    public RpcResult registerChain(ChainForm form) {
        validateForm(form);

        return RpcResult.success(null);
    }

    private String validateForm(ChainForm form) {
        if(!ValidateUtils.validateHash(form.getTxHash())) {
            return "[txHash is invalid]";
        }
        if(StringUtils.isBlank(form.getLogo())) {
            return "[logo] is invalid";
        }
        if(StringUtils.isBlank(form.getSymbol())) {
            return "[symbol] is invalid";
        }

        return null;

    }
}
