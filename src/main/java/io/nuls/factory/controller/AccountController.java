package io.nuls.factory.controller;

import io.nuls.factory.model.AccountInfo;
import io.nuls.factory.rpc.form.AccountForm;
import io.nuls.factory.rpc.result.RpcResult;
import io.nuls.factory.service.AccountService;
import io.nuls.factory.util.Log;
import io.nuls.factory.util.ValidateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/account")
@Component
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GET
    @Path("/register")
    @Produces(MediaType.APPLICATION_JSON)
    public RpcResult register(AccountForm form) {
        String address = form.getAddress();
        if (!ValidateUtils.validateAddress(address)) {
            return RpcResult.paramError("[address] is invalid");
        }
        try {
            AccountInfo accountInfo = accountService.getAccount(address);
            if (accountInfo == null) {
                accountInfo = new AccountInfo();
                accountInfo.setAddress(address);
                accountInfo = accountService.save(accountInfo);
            }
            return RpcResult.success(accountInfo);
        } catch (Exception e) {
            Log.error(e);
            return RpcResult.systemError(e.getMessage());
        }
    }

    @POST
    @Path("/select")
    @Produces(MediaType.APPLICATION_JSON)
    public RpcResult getAccount(AccountForm form) {
        String address = form.getAddress();
        if (!ValidateUtils.validateAddress(address)) {
            return RpcResult.paramError("[address] is invalid");
        }
        try {
            AccountInfo accountInfo = accountService.getAccount(address);
            if (accountInfo == null) {
                return RpcResult.dataNotFound("account not exist");
            }
            return RpcResult.success(accountInfo);
        } catch (Exception e) {
            Log.error(e);
            return RpcResult.systemError(e.getMessage());
        }
    }

}
