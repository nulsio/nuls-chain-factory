package io.nuls.factory.rpc.form;

public class AccountForm {

    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
