/*
 * MIT License
 * Copyright (c) 2017-2019 nuls.io
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package io.nuls.factory.rpc.result;

/**
 * @author Niels
 */
public enum RpcErrorCode {
    //系统未知错误
    SYS_UNKNOWN_EXCEPTION("1000", "System error!"),
    // 参数不对
    PARAMS_ERROR("1001", "Parameters is wrong!"),
    //数据未找到
    DATA_NOT_EXISTS("1002", "Data not found!"),
    // 合约未验证
    CONTRACT_NOT_VALIDATION_ERROR("1003", "Contract code not certified!"),

    // 合约已验证
    CONTRACT_VALIDATION_ERROR("1004", "The contract code has been certified!"),

    // 合约验证失败
    CONTRACT_VALIDATION_FAILED("1005", "Contract verification failed."),

    //脚本执行错误
    TX_SHELL_ERROR("1006", "Shell execute error!"),

    //交易解析错误
    TX_PARSE_ERROR("1010", "Transaction parse error!");


    private String code;

    private String message;

    RpcErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
