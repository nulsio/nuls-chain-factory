package io.nuls.factory.dao.util;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.datasource.unpooled.UnpooledDataSourceFactory;

public class DruidDataSourceBuilder extends UnpooledDataSourceFactory {
    public DruidDataSourceBuilder() {
        this.dataSource = new DruidDataSource();
    }
}
