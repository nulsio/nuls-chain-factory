package io.nuls.factory.dao.mapper;

import io.nuls.factory.model.AccountInfo;

@MyBatisMapper
public interface AccountInfoMapper {

    int deleteByPrimaryKey(String address);

    int insert(AccountInfo record);

    int insertSelective(AccountInfo record);

    AccountInfo selectByPrimaryKey(String address);

    int updateByPrimaryKeySelective(AccountInfo record);

    int updateByPrimaryKey(AccountInfo record);
}