package io.nuls.factory.dao.mapper;

import io.nuls.factory.model.ChainInfo;

@MyBatisMapper
public interface ChainInfoMapper {

    int deleteByPrimaryKey(Integer chainId);

    int insert(ChainInfo record);

    int insertSelective(ChainInfo record);

    ChainInfo selectByPrimaryKey(Integer chainId);

    int updateByPrimaryKeySelective(ChainInfo record);

    int updateByPrimaryKeyWithBLOBs(ChainInfo record);

    int updateByPrimaryKey(ChainInfo record);
}